CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Maintainers

INTRODUCTION
------------

The Wrapper Filter module provides a Filter plugin type that adds a wrapping
configurable div element around processed text output.


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.

INSTALLATION
------------

The recommended installation procedure is to use Composer.

composer require drupal/wrapper_filter

MAINTAINERS
-----------

Current maintainers:
 * Luke Leber <lukeleber@gmail.com> - https://www.drupal.org/u/lukeleber
