<?php

namespace Drupal\wrapper_filter\Plugin\Filter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Template\Attribute;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

/**
 * A simple filter plugin type that wraps a configurable div around output.
 *
 * @Filter(
 *   id = "wrapper_filter",
 *   title = @Translation("Wrapper filter"),
 *   description = @Translation("Adds a wrapper div around all processed text output"),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_REVERSIBLE
 * )
 */
class WrapperFilter extends FilterBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return parent::defaultConfiguration() + [
      'wrapper_classes' => 'wysiwyg',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $form = parent::settingsForm($form, $form_state);
    $form['wrapper_classes'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Wrapper element classes'),
      '#description' => $this->t('One class per line'),
      '#default_value' => $this->settings['wrapper_classes'],
      '#required' => TRUE,
      '#element_validate' => [[$this, 'validateWrapperClasses']],
      '#value_callback' => [[$this, 'getWrapperClasses']],
    ];

    return $form;
  }

  /**
   * Element validate callback for ensuring classes are safe to use.
   *
   * @param array $element
   *   The element to validate.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function validateWrapperClasses(array &$element, FormStateInterface $form_state): void {
    $classes_string = $form_state->getValue([
      'filters',
      'wrapper_filter',
      'settings',
      'wrapper_classes',
    ]);
    $classes = preg_split('~\R~', $classes_string);

    $errors = [];
    foreach ($classes as $class) {

      if ($class !== Html::cleanCssIdentifier($class)) {
        $errors[] = $class;
      }
    }

    if ($errors) {
      $form_state->setError($element, $this->t('For <a href="@link">security reasons</a>, the following class names are not allowed: %errors', [
        '@link' => 'https://www.drupal.org/docs/contributed-modules/wrapper-filter',
        '%errors' => implode(', ', $errors),
      ]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode): FilterProcessResult {

    $attributes = new Attribute();
    $classes = preg_split('~\R~', $this->settings['wrapper_classes']);
    $classes = array_filter($classes);
    foreach ($classes as $class) {
      $attributes->addClass(Html::cleanCssIdentifier($class));
    }

    return new FilterProcessResult("<div $attributes>$text</div>");
  }

}
